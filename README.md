# GrowAPair
Welcome.

The modlets contained here include Grow A Pair - Farm Life Expanded. A 'proof of concept' modlet featuring quality of life enhancements to add more features and content to the game. This started from our regular players
asking about old farming mods. I was asked if I could get these working for our server and this is my progress so far. I will also say... this is a learning process for myself, so things may be done differently.

FarmLifeMod2 and Farming Mod have been merged and some workstations and items are no longer used due to being duplicates, etc.
Unnecessary but Beautiful did not have any recipes at all in the copy I received. I've added simple recipes for all items for the time being.

I've added placeholder animals for more workstations and farming resources. The plans are to animate them in the future, but as they are now, they are stationary. Also, Not all new food items have actions applied at this time.


Special thanks to stasis78 (FarmLifeMod2), hernanxx (UnnecessarybutBeautiful) and Mayic (Farming Mod) for prior work on thier mods.

Extra special thanks to Linda, babygrl, Chris, Gator, Tru, Bluntz, David, Mistress Metal and Nomad for the initial help with everything to do with the farming mods.

Thank you to the 7 Days to Die modlet creators for adding so much content to the game.

Featured on our server:

Grow A Pair - Farm Life Expanded v0.0.0.1r2 (A combination of stasis78 FarmLifeMod2 + Mayic Farming Mod + hernanxx UnnecessarybutBeautiful modlets. Added more animals and a perk system for Farm Life unlocks.)

SoDead_WeaponMods_v0.0.0.5r2 (Includes a custom Skill/Perk system (Master Craftsman) for new Weapons, recipes and workstations.)


Have fun and enjoy your days surviving.

=====================================================================

For Easy installation, follow the directions on our Teamspeak server.

=====================================================================

To manually install mods for GrowAPair server.

Download the files in this repository. (Required) It will be called alpha-19-modlets-master.zip, then Extract this to a location of your choice.

1. Navigate to: (default install location) C:\Program Files (x86)\Steam\steamapps\common\7 Days To Die
2. It is Recommended to copy the 7 Days to Die folder and Paste that in another location of your choice.
3. Create a Mods folder in the new 7 Days to Die folder. (This MUST be named Mods)
4. Paste the folders in the Mods folder downloaded here into the Mods folder. (The names will be SoDead_GrowAPair_FarmLifeExpanded and SoDead_WeaponMods 'WeaponMods can be removed if you don't want to use it')
5. Navigate back to the main 7 Days to Die folder and select the 7DaysToDie_EAC.exe to start the game.